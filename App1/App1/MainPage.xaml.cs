﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Drawing;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data.MySqlClient;
using MySQLDemo.Classes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            startup();
        }

        private async void startup()
        {
            MessageDialog text = new MessageDialog($"\n\t Welcome to my project");
            text.Title = "Final Project";

            text.Commands.Add(new Windows.UI.Popups.UICommand($"Continue") { Id = 0 });

            text.DefaultCommandIndex = 0;

            var output = await text.ShowAsync();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.AddData(fName.Text, lName.Text, DOB.Text, strNum.Text, strName.Text, postCode.Text, cityName.Text, phoneNum1.Text, phoneNum2.Text, email.Text);
        }
        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.UpdateData(idNum.Text, fName.Text, lName.Text, DOB.Text, strNum.Text, strName.Text, postCode.Text, cityName.Text, phoneNum1.Text, phoneNum2.Text, email.Text);
        }
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.DeleteDate(idNum.Text);
        }
        private void resetButton_Click(object sender, RoutedEventArgs e)
        {

            idNum.Text = "";
            fName.Text = "";
            lName.Text = "";
            DOB.Text = "";
            strNum.Text = "";
            strName.Text = "";
            postCode.Text = "";
            cityName.Text = "";
            phoneNum1.Text = "";
            phoneNum2.Text = "";
            email.Text = "";
        }
    }
}
