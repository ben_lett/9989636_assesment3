﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "comp6002_week04";

        //DO NOT CHANGE THESE
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}"});
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }

      
        //////////////////    DB CLASS ///////////////////////////

        //CHANGE THIS!!
        //This class is specific to your DB
        public class AddPerson
        {
            public string fname { get; set; }
            public string lname { get; set; }
            public DateTime dob { get; set; }
            public int id { get; set; }
            public AddPerson(string _fname, string _lname, DateTime _dob)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _id)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                id = _id;
            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void AddData(string fname, string lname, string dob)
        {
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB)VALUES(@fname, @lname, @dob)";
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.ExecuteNonQuery();

            con.Clone();
        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void UpdateData(string fname, string lname, string dob, string id)
        {
            var uid = int.Parse(id);
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void DeleteDate(string id)
        {
            var uid = int.Parse(id);

            var customdb = new AddPerson(uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
